CREATE TABLE IF NOT EXISTS "person"
(
    id         BIGSERIAL NOT NULL
    CONSTRAINT person_pk PRIMARY KEY,
    first_name TEXT      NOT NULL,
    last_name TEXT      NOT NULL,
    age        integer      NOT NULL
);