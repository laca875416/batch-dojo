create table if not exists listing_status (
    id integer primary key,
    status_name text not null
);

create table if not exists marketplace (
    id integer primary key,
    marketplace_name text not null
);