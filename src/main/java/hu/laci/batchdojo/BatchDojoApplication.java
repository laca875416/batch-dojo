package hu.laci.batchdojo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchDojoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchDojoApplication.class, args);
	}

}
