package hu.laci.batchdojo.api.status.listener;

import hu.laci.batchdojo.api.status.model.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApiStatusListener implements ItemReadListener<Status> {
	
	@Override
	public void beforeRead() {
	}
	
	@Override
	public void afterRead(Status status) {
		log.info("Processing {} ", status);
	}
	
	@Override
	public void onReadError(Exception e) {
	}
}
