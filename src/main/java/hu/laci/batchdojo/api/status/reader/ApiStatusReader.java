package hu.laci.batchdojo.api.status.reader;

import hu.laci.batchdojo.api.reader.BufferedApiReader;
import hu.laci.batchdojo.api.status.model.Status;
import hu.laci.batchdojo.configuration.client.MockarooClient;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApiStatusReader extends BufferedApiReader<Status> {
	
	public ApiStatusReader(MockarooClient mockarooClient) {
		super(mockarooClient);
	}
	
	@Override
	protected List<Status> fetchItems() {
		return mockarooClient.getListingStatus(key);
	}
	
}
