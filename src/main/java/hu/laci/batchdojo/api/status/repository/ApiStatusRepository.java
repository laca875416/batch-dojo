package hu.laci.batchdojo.api.status.repository;

import hu.laci.batchdojo.api.status.model.Status;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ApiStatusRepository {
	
	private final NamedParameterJdbcTemplate jdbcTemplate;
	
	public void save(Status status) {
		jdbcTemplate.update(
				"insert into listing_status (id, status_name) values (:id, :name) on conflict do nothing ",
				new BeanPropertySqlParameterSource(status)
		);
	}
	
}
