package hu.laci.batchdojo.api.marketplace.repository;

import hu.laci.batchdojo.api.marketplace.model.Marketplace;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ApiMarketplaceRepository {
	
	private final NamedParameterJdbcTemplate jdbcTemplate;
	
	public void save(Marketplace marketplace) {
		jdbcTemplate.update(
				"insert into marketplace (id, marketplace_name) values (:id, :name) on conflict do nothing ",
				new BeanPropertySqlParameterSource(marketplace)
		);
	}
	
}
