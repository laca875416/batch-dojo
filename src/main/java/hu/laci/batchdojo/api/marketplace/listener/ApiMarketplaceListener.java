package hu.laci.batchdojo.api.marketplace.listener;

import hu.laci.batchdojo.api.marketplace.model.Marketplace;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApiMarketplaceListener implements ItemReadListener<Marketplace> {
	
	@Override
	public void beforeRead() {
	}
	
	@Override
	public void afterRead(Marketplace marketplace) {
		log.info("Processing {} ", marketplace);
	}
	
	@Override
	public void onReadError(Exception e) {
	}
}
