package hu.laci.batchdojo.api.marketplace.writer;

import hu.laci.batchdojo.api.marketplace.model.Marketplace;
import hu.laci.batchdojo.api.marketplace.repository.ApiMarketplaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ApiMarketplaceWriter implements ItemWriter<Marketplace> {
	
	private final ApiMarketplaceRepository apiMarketplaceRepository;
	
	@Override
	public void write(List<? extends Marketplace> marketplaces) throws Exception {
		marketplaces.forEach(apiMarketplaceRepository::save);
	}
}
