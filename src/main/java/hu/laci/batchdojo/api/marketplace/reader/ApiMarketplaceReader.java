package hu.laci.batchdojo.api.marketplace.reader;

import hu.laci.batchdojo.api.marketplace.model.Marketplace;
import hu.laci.batchdojo.api.reader.BufferedApiReader;
import hu.laci.batchdojo.configuration.client.MockarooClient;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApiMarketplaceReader extends BufferedApiReader<Marketplace> {
	
	public ApiMarketplaceReader(MockarooClient mockarooClient) {
		super(mockarooClient);
	}
	
	@Override
	protected List<Marketplace> fetchItems() {
		return mockarooClient.getMarketplace(key);
	}
	
}
