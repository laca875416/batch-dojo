package hu.laci.batchdojo.personstore.reader;

import hu.laci.batchdojo.personstore.model.Person;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;

@Configuration
public class PersonStoreReaderConfiguration {
	
	private String csvPath;
	
	@Bean
	@StepScope
	public FlatFileItemReader<Person> batchDojoCsvReader() {
		return new FlatFileItemReaderBuilder<Person>()
				.name("batchDojoCsvReader")
				.resource(new PathResource(csvPath))
				.lineMapper(provideLineMapper())
				.linesToSkip(1)
				.build();
	}
	
	private DefaultLineMapper<Person> provideLineMapper() {
		DefaultLineMapper<Person> lineMapper = new DefaultLineMapper<>();
		lineMapper.setLineTokenizer(provideTokenizer());
		lineMapper.setFieldSetMapper(provideFieldSetMapper());
		return lineMapper;
	}
	
	private DelimitedLineTokenizer provideTokenizer() {
		DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
		tokenizer.setDelimiter(DelimitedLineTokenizer.DELIMITER_TAB);
		tokenizer.setNames("lastName", "firstName", "age");
		return tokenizer;
	}
	
	private FieldSetMapper<Person> provideFieldSetMapper() {
		BeanWrapperFieldSetMapper<Person> personBeanWrapperFieldSetMapper = new BeanWrapperFieldSetMapper<>();
		personBeanWrapperFieldSetMapper.setTargetType(Person.class);
		return personBeanWrapperFieldSetMapper;
	}
	
	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		csvPath = stepExecution
				.getJobExecution()
				.getExecutionContext()
				.getString("csvPath");
	}
}
