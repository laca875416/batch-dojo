package hu.laci.batchdojo.personstore;

import hu.laci.batchdojo.personstore.listener.PersonStoreListener;
import hu.laci.batchdojo.personstore.model.Person;
import hu.laci.batchdojo.personstore.processor.PersonStoreProcessor;
import hu.laci.batchdojo.personstore.reader.PersonStoreReaderConfiguration;
import hu.laci.batchdojo.personstore.writer.PersonStoreWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class PersonStoreStepConfiguration {
	
	private final StepBuilderFactory stepBuilderFactory;
	private final FlatFileItemReader<Person> batchDojoCsvReader;
	private final PersonStoreProcessor personStoreProcessor;
	private final PersonStoreWriter personStoreWriter;
	private final PersonStoreListener csvListener;
	private final PersonStoreReaderConfiguration personStoreReaderConfiguration;
	
	@Bean
	public Step personStoreStep() {
		return stepBuilderFactory
				.get("personStoreStep")
				.<Person, Person>chunk(1)
				.reader(batchDojoCsvReader)
				.listener(personStoreReaderConfiguration)
				.processor(personStoreProcessor)
				.listener(csvListener)
				.writer(personStoreWriter)
				.faultTolerant()
				.skipLimit(3)
				.skip(Exception.class)
				.build();
	}
	
}
