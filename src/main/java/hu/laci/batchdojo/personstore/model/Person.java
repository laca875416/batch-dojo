package hu.laci.batchdojo.personstore.model;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person {
	private Long id;
	private String lastName;
	private String firstName;
	private int age;
	
	
	
}
