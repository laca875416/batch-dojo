package hu.laci.batchdojo.personstore.listener;

import hu.laci.batchdojo.personstore.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PersonStoreListener implements ItemProcessListener<Person, Person> {
	
	@Override
	public void beforeProcess(Person person) {
	}
	
	@Override
	public void afterProcess(Person person, Person newPerson) {
		log.info("Old person: {} change to {}", person, newPerson);
	}
	
	@Override
	public void onProcessError(Person person, Exception e) {
	}
}
