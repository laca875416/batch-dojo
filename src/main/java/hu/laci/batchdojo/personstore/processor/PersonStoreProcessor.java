package hu.laci.batchdojo.personstore.processor;

import hu.laci.batchdojo.personstore.model.Person;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class PersonStoreProcessor implements ItemProcessor<Person, Person> {
	
	private static final String SUB_FIX = "_test";
	
	@Override
	public Person process(Person person) {
		return createNewPersonBuilder(person).build();
	}
	
	private Person.PersonBuilder createNewPersonBuilder(Person person) {
		return Person
				.builder()
				.firstName(generateFirstName(person))
				.lastName(person.getLastName())
				.age(person.getAge());
	}
	
	private String generateFirstName(Person person) {
		return person.getFirstName().concat(SUB_FIX);
	}
	
	
}
