package hu.laci.batchdojo.personstore.repository;


import hu.laci.batchdojo.personstore.model.Person;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class PersonRepository {
	
	private final NamedParameterJdbcTemplate jdbcTemplate;
	
	private String getStorePersonToDatabaseQuery() {
		return "INSERT INTO person (first_name, last_name, age) values (:firstName, :lastName, :age) ON CONFLICT DO NOTHING";
	}
	
	public void save(Person person) {
		jdbcTemplate.update(
				getStorePersonToDatabaseQuery(),
				new BeanPropertySqlParameterSource(person));
	}
	
}