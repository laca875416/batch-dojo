package hu.laci.batchdojo.personstore.writer;

import hu.laci.batchdojo.personstore.model.Person;
import hu.laci.batchdojo.personstore.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class PersonStoreWriter implements ItemWriter<Person> {
	
	private final PersonRepository personRepository;
	
	@Override
	public void write(List<? extends Person> persons) {
		persons.forEach(personRepository::save);
	}
	
}