package hu.laci.batchdojo;

import hu.laci.batchdojo.common.CustomExitStatus;
import hu.laci.batchdojo.listener.JobListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.support.SimpleFlow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
@EnableFeignClients
@RequiredArgsConstructor
@Slf4j
public class BatchDojoJobConfiguration {
	
	private final JobBuilderFactory jobBuilderFactory;
	private final JobListener jobListener;
	private final Step personStoreStep;
	private final Step dumpSourceStep;
	private final Step remoteDumpStep;
	private final Step remoteLoadStep;
	private final Step apiMarketplaceStep;
	private final Step apiStatusStep;
	private final Step ftpUploaderStep;
	
	@Bean
	public Job batchDojoJob() {
		log.info("Creating job.");
		return jobBuilderFactory
				.get("batchDojoJob")
				.incrementer(new RunIdIncrementer())
				.start(remoteDumpStep)
				.next(remoteLoadStep)
				.on(CustomExitStatus.FTP.getExitCode()).to(ftpUploaderStep).next(remainingSteps()).from(remoteLoadStep)
				.on(ExitStatus.COMPLETED.getExitCode()).to(remainingSteps()).from(remoteLoadStep).end()
				.listener(jobListener)
				.build();
	}
	
	private Flow remainingSteps() {
		return new FlowBuilder<SimpleFlow>("smart-ftp-flow")
				.start(dumpSourceStep)
				.next(personStoreStep)
				.next(apiMarketplaceStep)
				.next(apiStatusStep)
				.end();
	}
	
}
