package hu.laci.batchdojo.dump.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DumpSourceStepListener implements StepExecutionListener {
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		log.info("Dumping source starting"); // To demonstrate StepExecutionListener.
	}
	
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		log.info("Dumping source finished");
		return ExitStatus.COMPLETED;
	}
}
