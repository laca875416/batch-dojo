package hu.laci.batchdojo.dump.writer;

import hu.laci.batchdojo.dump.model.Source;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.item.file.FlatFileHeaderCallback;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import java.time.LocalDateTime;

import static org.springframework.batch.item.file.transform.DelimitedLineTokenizer.DELIMITER_TAB;

@Configuration
public class DumpSourceWriterConfiguration {
	
	private static final String HEADER = "last_name\tfirst_name\tage";
	
	@Value("${filePath}")
	private String filePath;
	private String csvPath;
	
	@Bean
	public FlatFileItemWriter<Source> dumpSourceWriter() {
		csvPath = generateCsvPath();
		
		return new FlatFileItemWriterBuilder<Source>()
				.name("dumpSourceWriter")
				.resource(new FileSystemResource(csvPath))
				.lineAggregator(initLineAggregator())
				.headerCallback(initHeader())
				.build();
	}
	
	//The only reason why I generate the csv path is to demonstrate AfterStep and StepScope (PersonStoreReaderConfiguration) features.
	private String generateCsvPath() {
		return filePath + LocalDateTime.now().toString().replace(":", "_") + ".csv";
	}
	
	private DelimitedLineAggregator<Source> initLineAggregator() {
		DelimitedLineAggregator<Source> aggregator = new DelimitedLineAggregator<>();
		aggregator.setDelimiter(DELIMITER_TAB);
		aggregator.setFieldExtractor(initFieldExtractor());
		return aggregator;
	}
	
	private BeanWrapperFieldExtractor<Source> initFieldExtractor() {
		BeanWrapperFieldExtractor<Source> extractor = new BeanWrapperFieldExtractor<>();
		extractor.setNames(new String[]{"lastName", "firstName", "age"});
		return extractor;
	}
	
	private FlatFileHeaderCallback initHeader() {
		return writer -> writer.write(HEADER);
	}
	
	@AfterStep
	public void afterStep(StepExecution stepExecution) {
		stepExecution
				.getJobExecution()
				.getExecutionContext()
				.put("csvPath", csvPath);
	}
	
}
