package hu.laci.batchdojo.dump;

import hu.laci.batchdojo.dump.listener.DumpSourceStepListener;
import hu.laci.batchdojo.dump.model.Source;
import hu.laci.batchdojo.dump.writer.DumpSourceWriterConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class DumpSourceStepConfiguration {
	
	private final StepBuilderFactory stepBuilderFactory;
	private final JdbcPagingItemReader<Source> dumpSourceReader;
	private final FlatFileItemWriter<Source> dumpSourceWriter;
	private final DumpSourceWriterConfiguration dumpSourceWriterConfiguration;
	private final DumpSourceStepListener dumpSourceStepListener;
	
	@Bean
	public Step dumpSourceStep() {
		return stepBuilderFactory
				.get("dumpSourceStep")
				.<Source, Source>chunk(100)
				.faultTolerant()
				.skipLimit(3)
				.skip(Exception.class)
				.reader(dumpSourceReader)
				.writer(dumpSourceWriter)
				.listener(dumpSourceWriterConfiguration)
				.listener(dumpSourceStepListener)
				.build();
	}
	
}
