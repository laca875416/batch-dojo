package hu.laci.batchdojo.dump.reader;

import hu.laci.batchdojo.dump.model.Source;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.builder.JdbcPagingItemReaderBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.Map;

@Configuration
public class DumpSourceReaderConfiguration {
	
	private static final String SOURCE_COLUMNS = "id, first_name, last_name, age";
	private static final String SOURCE_TABLE_NAME = "source";
	private static final int DEFAULT_SIZE = 100;
	
	@Bean
	public JdbcPagingItemReader<Source> dumpSourceReader(DataSource dataSource) {
		JdbcPagingItemReaderBuilder<Source> builder = new JdbcPagingItemReaderBuilder<Source>()
				.name("dumpSourceReader")
				.rowMapper(getRowMapper())
				.selectClause(getSelectClause())
				.fromClause(getFromClause())
				.sortKeys(getSortKey())
				.fetchSize(getFetchSize())
				.pageSize(getPageSize())
				.dataSource(dataSource);
		return builder.build();
	}
	
	private RowMapper<Source> getRowMapper() {
		return BeanPropertyRowMapper.newInstance(Source.class);
	}
	
	private String getSelectClause() {
		return SOURCE_COLUMNS;
	}
	
	private String getFromClause() {
		return SOURCE_TABLE_NAME;
	}
	
	/**
	 * Be aware, that the sortKey could possibly hide elements because of paging, so choose columns wisely.
	 * For example:
	 * If you put only OrderId in the sortKey map, then it will get getPageSize() amount of line from the database
	 * in the first loop. Then the second loop, it will take getPageSize() amount and spring will put this in your sql:
	 * (and OrderId > ?).
	 * So if you have higher amount of lines with the same OrderId than getPageSize(), some lines will disappear.
	 *
	 * You should use multiple value. Example OrderId and ProductId. So in this case the paging will look like this:
	 * (and OrderId > ? and ProductId > ?). With this solution you can avoid data loss.
	 *
	 * Also, don't put columns inside the map, which are not in the Select clause, because it won't find that column.
	 */
	public Map<String, Order> getSortKey() {
		return Map.of("id", Order.ASCENDING);
	}
	
	private int getFetchSize() {
		return DEFAULT_SIZE;
	}
	
	private int getPageSize() {
		return DEFAULT_SIZE;
	}
	
}
