package hu.laci.batchdojo.personstore.processor;

import hu.laci.batchdojo.personstore.model.Person;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BatchDojoCsvProcessorTest {
	
	@Test
	void testProcessor() {
		PersonStoreProcessor personStoreProcessor = new PersonStoreProcessor();
		Person person = initPerson();
		
		Person processedPerson = personStoreProcessor.process(person);
		
		assertNotNull(processedPerson);
		assertEquals("first_test", processedPerson.getFirstName());
		assertEquals("last", processedPerson.getLastName());
		assertEquals(1, processedPerson.getAge());
	}
	
	private Person initPerson() {
		return Person
				.builder()
				.firstName("first")
				.lastName("last")
				.age(1)
				.build();
	}
	
}
