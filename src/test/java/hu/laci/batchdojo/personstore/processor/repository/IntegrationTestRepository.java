package hu.laci.batchdojo.personstore.processor.repository;

import hu.laci.batchdojo.api.marketplace.model.Marketplace;
import hu.laci.batchdojo.api.status.model.Status;
import hu.laci.batchdojo.dump.model.Source;
import hu.laci.batchdojo.personstore.model.Person;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class IntegrationTestRepository {
	
	private final NamedParameterJdbcTemplate jdbcTemplate;
	
	public List<Marketplace> fetchMarketplaces() {
		return jdbcTemplate.query(
				"select id, marketplace_name as name from marketplace",
				new BeanPropertyRowMapper<>(Marketplace.class)
		);
	}
	
	public List<Status> fetchStatuses() {
		return jdbcTemplate.query(
				"select id, status_name as name from listing_status",
				new BeanPropertyRowMapper<>(Status.class)
		);
	}
	
	public Source fetchSourceData(Integer id) {
		return jdbcTemplate.query(
						"select * from source where id=:id",
						new MapSqlParameterSource("id", id),
						new BeanPropertyRowMapper<>(Source.class)
				)
				.stream()
				.findFirst()
				.orElse(null);
	}
	
	public Person fetchPersonData(Integer id) {
		return jdbcTemplate.query(
						"select * from person where id=:id",
						new MapSqlParameterSource("id", id),
						new BeanPropertyRowMapper<>(Person.class)
				)
				.stream()
				.findFirst()
				.orElse(null);
	}
	
	public Integer fetchTableCount(String table) {
		return jdbcTemplate.queryForObject(
				"select count(*) from " + table,
				EmptySqlParameterSource.INSTANCE,
				Integer.class
		);
	}
	
}
