package hu.laci.batchdojo.personstore.processor.integrationtest;

import hu.laci.batchdojo.configuration.client.MockarooClient;
import hu.laci.batchdojo.dump.model.Source;
import hu.laci.batchdojo.ftp.FtpUploaderTasklet;
import hu.laci.batchdojo.personstore.model.Person;
import hu.laci.batchdojo.personstore.processor.repository.IntegrationTestRepository;
import hu.laci.batchdojo.personstore.processor.stub.MarketplaceStub;
import hu.laci.batchdojo.personstore.processor.stub.StatusStub;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class IntegrationTest {
	
	@MockBean
	private MockarooClient client;
	@MockBean
	private FtpUploaderTasklet ftpUploaderTasklet;
	@Value("${mockaroo.key}")
	private String clientKey;
	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;
	@Autowired
	private IntegrationTestRepository repository;
	
	
	@Test
	public void testJob() throws Exception {
		when(client.getListingStatus(clientKey)).thenReturn(StatusStub.getListingStatusStubs());
		when(client.getMarketplace(clientKey)).thenReturn(MarketplaceStub.getMarketplaceStubs());
		when(ftpUploaderTasklet.execute(any(), any())).thenReturn(RepeatStatus.FINISHED);
		
		runJob();
		testLoadFileIntoPrimaryDs();
		testMoveDataBetweenTables();
		testMarketplaceStoring();
		testStatusStoring();
	}
	
	private void testStatusStoring() {
		assertTrue(
				CollectionUtils.isEqualCollection(
						repository.fetchStatuses(),
						StatusStub.getListingStatusStubs()
				)
		);
	}
	
	private void testMarketplaceStoring() {
		assertTrue(
				CollectionUtils.isEqualCollection(
						repository.fetchMarketplaces(),
						MarketplaceStub.getMarketplaceStubs()
				)
		);
	}
	
	private void testLoadFileIntoPrimaryDs() {
		assertEquals(30, repository.fetchTableCount("source").intValue());
		Source source = repository.fetchSourceData(1);
		assertNotNull(source);
		assertEquals("Teszt",source.getFirstName());
		assertEquals("Elek",source.getLastName());
		assertEquals(30,source.getAge());
	}
	
	private void testMoveDataBetweenTables() {
		assertEquals(30, repository.fetchTableCount("person").intValue());
		Person person = repository.fetchPersonData(1);
		assertNotNull(person);
		assertEquals("Teszt_test",person.getFirstName());
		assertEquals("Elek",person.getLastName());
		assertEquals(30,person.getAge());
	}
	
	private void runJob() throws Exception {
		JobParameters jobParameters = jobLauncherTestUtils.getUniqueJobParameters();
		JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
		
		assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
		assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
	}
	
}
