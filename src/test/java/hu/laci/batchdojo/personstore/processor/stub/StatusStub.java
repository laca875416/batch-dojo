package hu.laci.batchdojo.personstore.processor.stub;

import hu.laci.batchdojo.api.status.model.Status;

import java.util.ArrayList;
import java.util.List;

public class StatusStub {
	
	public static List<Status> getListingStatusStubs() {
		List<Status> list = new ArrayList<>();
		list.add(Status.builder().id(1).name("ACTIVE").build());
		list.add(Status.builder().id(2).name("SCHEDULED").build());
		list.add(Status.builder().id(3).name("CANCELLED").build());
		return list;
	}
	
}
